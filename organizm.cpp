#include "organizm.hpp"

Organizm::Organizm(string text)
{
	if (text.size() == ORGANISM_LENGTH) {
		this->text = text;
	} else {
		string errorMessage = "Organizm nie posiada oczekiwanej dlugości";
		throw errorMessage;
	}
}

string Organizm::wezGenotyp()
{
	return this->text;
}
