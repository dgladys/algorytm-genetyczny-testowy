#ifndef organizm_hpp
#define organizm_hpp

#include <iostream>

const int ORGANISM_LENGTH = 8;

using namespace std;

/**
 * Class Organizm
 * Klasa przechowuje informacje o pojedynczym organizmie
 * */
class Organizm
{
	protected:
		string text;
	
	public:
		/**
		 * Konstruktor który tworzy organizm po zweryfikowaniu poprawności
		 * danych wejściowych
		 * */
		Organizm(string text);
		
		/**
		 * Zwraca genotyp w formie zakodowanej
		 * */
		string wezGenotyp();
};

#endif
