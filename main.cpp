#include <iostream>
#include <ctime>
#include <cstdlib>
#include "organizm.hpp"
#include <list>
#include <vector>
#include <sstream>
#include <windows.h>
#include <cstdio>

using namespace std;

/**
 * Convert int to string
 * @return string
 * */
string IntToString (int a)
{
    ostringstream temp;
    temp<<a;
    return temp.str();
}

/**
 * Class OrganizmGenerator
 * Umożliwia wygenerowanie pewnej populacji.
 * */
class OrganizmGenerator
{
	
	public:
	/**
	 * Generuje losowy nowy organizm
	 * */
	static Organizm *generuj()
	{
		string genotyp = "";
		for (short i = 0; i < ORGANISM_LENGTH; i++) {
			char gen = 48 + (rand() % 10);
			genotyp += gen;
		}
		return new Organizm(genotyp);
	}
	
	static list<Organizm *> generujPopulacje (int ilosc_populacji = 8)
	{
		list<Organizm *> lista;
		for (short i = 0; i < ilosc_populacji; i++) {
			lista.push_back(OrganizmGenerator::generuj());
		}
		return lista;
	}
	
};

/**
 * Klasa dostarczająca funkcjonalności umożliwiającej ocenę organizmu
 * w porównaniu do jego pozostałych
 * */
class OcenaOrganizmu
{
	public:
	
	/**
	 * Metoda zwraca ocene w formie liczbowej okreslajaca
	 * zdolnosc przetrwania organizmu. Nabiera ona znaczeniu
	 * w momencie przyrównania do pewnej populacji.
	 * @return int
	 * */
	static int zdolnoscPrzetrwania(Organizm *organizm)
	{
		int ocena = 0;
		string genotyp = organizm->wezGenotyp();
		for (short i = 0; i < ORGANISM_LENGTH; i++)
		{
			ocena += ((int)genotyp[i]) - ((int)'0');
		}
		return ocena;
	}
	
	static int zdolnoscPrzetrwaniaPopulacji(list<Organizm*> populacja)
	{
		int ocena = 0;
		for (list<Organizm*>::iterator organizm = populacja.begin(); organizm != populacja.end(); organizm++) {
			ocena += OcenaOrganizmu::zdolnoscPrzetrwania(*organizm);
		}
		return ocena;
	}
};

/**
 * Class OperacjaKrzyzowanie
 * Obiekt umożliwia krzyżowanie dwóch organizmów, co daje początek nowemu
 * */
class OperacjaKrzyzowanie
{
	/**
	 * Losuje punkt przedziału wg. którego dzieli genotypy
	 * Jest to wartość z przedziału (0, ORGANISM_LENGTH)
	 * @return short int
	 * */
	short losujPunktPrzedzialu();
	
	public:
	
	/**
	 * Krzyzuje dwa organizmy i generuje dwóch potomków
	 * @return list<Organizm*>
	 * */
	list<Organizm*> krzyzuj(Organizm *o1, Organizm *o2);
	
};

class Selekcja {
	
		list<Organizm*> populacja;
	
	public:
	
		Selekcja(list<Organizm *> populacja)
		{
			int ocenaPopulacji = OcenaOrganizmu::zdolnoscPrzetrwaniaPopulacji(populacja);
			int populacja_size = populacja.size();
			list<Organizm *> wyselekcjowanaPopulacja;
			while (wyselekcjowanaPopulacja.size() < (populacja_size / 2)) {
				for (list<Organizm*>::iterator organizm = populacja.begin(); organizm != populacja.end() && wyselekcjowanaPopulacja.size() < (populacja_size / 2); organizm++) {
					Organizm *organizmPopulacji = (*organizm);
					int ocenaOsobnika = OcenaOrganizmu::zdolnoscPrzetrwania(organizmPopulacji);
					int los = rand() % ocenaPopulacji;
					if (los <= ocenaOsobnika) {
						wyselekcjowanaPopulacja.push_back(organizmPopulacji);
						organizm = populacja.erase(organizm);
						
					}
				}
			}
			this->populacja = wyselekcjowanaPopulacja;
		}
		
		list<Organizm*> wezPopulacje()
		{
			return this->populacja;
		}
	
};

class Krzyzowanie
{
	public:
		static list<Organizm *> krzyzuj(list<Organizm *> populacja)
		{
			int ilosc_dzieci = populacja.size();
			int liczebnosc_populacji = populacja.size();
			list<Organizm*> dzieci;
			OperacjaKrzyzowanie krzyzowanie;
			vector<Organizm*> wektorPopulacji;
			copy(populacja.begin(),populacja.end(),back_inserter(wektorPopulacji));
			while (dzieci.size() < ilosc_dzieci) {
				int rand1 = rand() % liczebnosc_populacji;
				int rand2 = rand1;
				while(rand1 == rand2) {
					rand2 = rand() % liczebnosc_populacji;
				}
				list<Organizm *> dzieciCykl;
				dzieciCykl = krzyzowanie.krzyzuj(wektorPopulacji[rand1], wektorPopulacji[rand2]);
				dzieci.push_back(wektorPopulacji[rand1]);
				if (dzieci.size() < ilosc_dzieci) {
					dzieci.push_back(wektorPopulacji[rand2]);
				}
			}
			return dzieci;
		}
};

class Populacja {
	
	public:
	
	static void czytaj(list<Organizm*> populacja) {
		for (list<Organizm*>::iterator organizm = populacja.begin(); organizm != populacja.end(); organizm++) {
			int wartoscOceny = OcenaOrganizmu::zdolnoscPrzetrwania(*organizm);
			string ocena = IntToString(wartoscOceny);
			cout << "Organizm: " << (*organizm)->wezGenotyp() << " - " << ocena << endl;
		}
	}
};

int main()
{
	srand(time(NULL));
	/*Organizm *organizm1 = OrganizmGenerator::generuj();
	Organizm *organizm2 = OrganizmGenerator::generuj();
	
	cout << organizm1 -> wezGenotyp() << endl;
	cout << organizm2 -> wezGenotyp() << endl << endl;
	
	OperacjaKrzyzowanie krzyzowanie;
	list<Organizm*> dzieci;
	dzieci = krzyzowanie.krzyzuj(organizm1, organizm2);
	
	for (list<Organizm*>::iterator it = dzieci.begin(); it != dzieci.end(); it++) {
		cout << "Dziecko: " << (*it)->wezGenotyp() << endl;
	}
	
	for (int i = 0; i < 5; i++) {
		
	}*/
	
	list<Organizm*> populacja = OrganizmGenerator::generujPopulacje(32);
	for (int i = 0; i < 500; i++) {
		Selekcja selekcjaPopulacji(populacja);
		list<Organizm*> selekcja = selekcjaPopulacji.wezPopulacje();
		list<Organizm*> krzyzowanie = Krzyzowanie::krzyzuj(selekcja);
		selekcja.merge(krzyzowanie);
		populacja = selekcja;
	}

	cout << "[Populacja]" << populacja.size() << endl;
	Populacja::czytaj(populacja);
	

	return 0;
}

/**
 * Krzyzuje dwa organizmy i tworzy nowego osobnika
 *
 * */
list<Organizm*> OperacjaKrzyzowanie::krzyzuj(Organizm *o1, Organizm *o2)
{
	string g1 = o1->wezGenotyp();
	string g2 = o2->wezGenotyp();
	
	short punktPrzedzialu = this->losujPunktPrzedzialu();
	
	string g1_first_part = g1.substr(0, punktPrzedzialu);
	string g1_second_part = g1.substr(punktPrzedzialu);
	
	string g2_first_part = g2.substr(0,punktPrzedzialu);
	string g2_second_part = g2.substr(punktPrzedzialu);
	
	/**
	 * Powstają w tym miejscu dwa pełne genotypy nowych osobników
	 * */
	string new_organizm_1 = g1_first_part + g2_second_part;
	string new_organizm_2 = g2_first_part + g1_second_part;
	
	list <Organizm *> organizmList;
	organizmList.push_back(new Organizm(new_organizm_1));
	organizmList.push_back(new Organizm(new_organizm_2));
	
	return organizmList;
}

short OperacjaKrzyzowanie::losujPunktPrzedzialu()
{
	int offset = (rand() % ORGANISM_LENGTH)-1;
	return 1 + (offset >= 0 ? offset : 0);
}
